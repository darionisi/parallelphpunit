import commands
import re

def executeCommand(cmd, validate = True, output=False):
    '''High level execution of a command. Return only the exit code, instead of the entire output. Validate the command before execute it'''
    if validateCommand(cmd) != True and validate == True:
        return -1

    status = commands.getstatusoutput(cmd)

    if output == True:
        print(status[1])

    status = status[0]
    status = status >> 8
    return status

def validateCommand(cmd):
    '''Check that the string cmd doesn't contain dangerous shell meta-characters that couold allow remote user t execute arbitrary code'''
    if re.match(".*[#&;`|*?~<>^()[\]{}$].*" , cmd) == None:
        return True
    return False

def executeCommandAndGetOutput(cmd, validate = True, output = False):
	'''High level execution of a command. Return the entire output. Validate the command before execute it'''
	if validateCommand(cmd) != True and validate == True:
		return -1

	status = commands.getstatusoutput(cmd)

	if output == True:
		print(status[1])

   	return status