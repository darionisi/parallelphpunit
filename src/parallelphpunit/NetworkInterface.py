'''
Created on 08/mag/2014

@author: dario
'''


class NetworkInterface(object):
    '''
    NetworkInterface:
        defines the bases for comunication beetween Server and Client.
        Contains constant values for command and methods prototype that Client and Server have to implement
    '''
    CONNECT_REQUEST = "1"
    CONNECT_ACCEPT = "2"
    CONNECT_REFUSED = "3"
    BRANCH_SETTING_REQUEST = "4"
    BRANCH_SETTING_SUCCEED = "5"
    BRANCH_SETTING_FAILED = "6"
    TEST_SELECTION_REQUEST = "7"
    TEST_SELECTION_SUCCEED = "8"
    TEST_SELECTION_FAILED = "9"
    TEST_EXECUTION_REQUEST = "10"
    TEST_EXECUTION_OK = "11"
    TEST_EXECUTION_FAILED = "12"
    RESULT_REQUEST = "13"
    RESULT_RESPONSE = "14"
    CLOSE_CONNECTION = "-1"
    
    SERVER_PORT = 2048