import sys
import os
import threading
import time

sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../"))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../.."))

import yaml
import pprint
from Slave import Slave
from Server import ServerThread
from threading import Semaphore

f = open('config.yml')
config = yaml.safe_load(f)

test_root = config['test_root']
os.chdir(test_root)

sem_slave = Semaphore(0)
sem_server = Semaphore(0)

slave = Slave(test_root, sem_slave, sem_server)
server_thread = ServerThread(slave, sem_slave, sem_server)
server_thread.setDaemon(True)
slave.setDaemon(True)
server_thread.start()
slave.start()

try:
    while threading.active_count() > 0:
        time.sleep(0.1)
except KeyboardInterrupt:
    pass