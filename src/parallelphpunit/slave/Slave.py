'''
Created on 10/mag/2014

@author: Dario Nisi
'''
import safecommands
import threading

class Slave(threading.Thread):
    '''
    Slave:
        definition of the main features of the slave
    '''

    BRANCH_SELECTION = 1
    BRANCH_OK = 2
    TEST_SELECTION = 3
    TEST_OK = 4
    TEST_EXEC = 5
    TEST_EXEC_OK = 6
    RESULT = 7
    GENERIC_ERROR = -1
    
    def __init__(self, test_root, sem1, sem2):
        '''
        Constructor
        '''
        threading.Thread.__init__(self)
        self.test_root = test_root
        self.semSlave = sem1
        self.semServer = sem2
        self.request = -1
        self.data = ""
        self.response = ""
        self.branch = ""
        self.test = ""
        self.testResult = ""

    def setRequest(self, req):
        ''' Setter for the request. It should be invoked by the network process'''
        self.request = req

    def setData(self, data):
        '''Setter for the data. It should be invoked by the network process.'''
        self.data = data

    def readRequest(self):
        '''Wait for a new request from the network process, then return request type and data provided '''
        self.semSlave.acquire()
        return self.request, self.data

    def getResponse(self):
        '''Getter for the response. This method should be locked by a semaphore, in order to synchronize the network process'''
        return self.response
    
    def provideResponse(self):
        '''Unlock the network process that is waiting for the response of a request'''
        self.semServer.release()

    def switchBranch(self):
        '''Run git commands that change the branch of the repository. Returns 0 if OK.'''

        print('\033[93m' + "git fetch origin" + '\033[39m')
        if safecommands.executeCommand("git fetch origin", True, True) != 0:
            return 1

        print('\033[93m' + "git checkout " + self.branch + '\033[39m')
        if safecommands.executeCommand("git checkout " + self.branch, True, True) != 0:
            return 1

        print('\033[93m' + "git reset --hard origin/" + self.branch + '\033[39m')
        if safecommands.executeCommand("git reset --hard origin/" + self.branch, True, True) != 0:
            return 1

        print('\033[93m' + "sed -i \"s/database_user: \\(.*\\)$/database_user: $MYSQL_USER/g\" app/config/parameters.yml" + '\033[39m')
        if safecommands.executeCommand("sed -i \"s/database_user: \\(.*\\)$/database_user: $MYSQL_USER/g\" app/config/parameters.yml", False, True) != 0:
            return 1

        print('\033[93m' + "sed -i \"s/database_password: \\(.*\\)$/database_password: $MYSQL_PASSWORD/g\" app/config/parameters.yml" + '\033[39m')
        if safecommands.executeCommand("sed -i \"s/database_password: \\(.*\\)$/database_password: $MYSQL_PASSWORD/g\" app/config/parameters.yml", False, True) != 0:
            return 1

        print('\033[93m' + "sed -i \"s/database_name: \\(.*\\)$/database_name: test${TEST_ENV_NUMBER}/g\" app/config/parameters.yml" + '\033[39m')
        if safecommands.executeCommand("sed -i \"s/database_name: \\(.*\\)$/database_name: test${TEST_ENV_NUMBER}/g\" app/config/parameters.yml", False, True) != 0:
            return 1

        print('\033[93m' + "sed -i \"s#php app/console fos:js-routing:dump --target web/assets/js/routes.js##g\" composer.json" + '\033[39m')
        if safecommands.executeCommand("sed -i \"s#php app/console fos:js-routing:dump --target web/assets/js/routes.js##g\" composer.json", False, True) != 0:
            return 1

        print('\033[93m' + "composer install --prefer-source --no-interaction" + '\033[39m')
        if safecommands.executeCommand("composer install --prefer-source --no-interaction") != 0:
            return 1

        print('\033[93m' + 'rm -rf data/{fakes,shared,uploads}/*' + '\033[39m')
        if safecommands.executeCommand("rm -rf data/{fakes,shared,uploads}/*", False) != 0:
            return 1

        print('\033[93m' + 'rm -rf web/{fakes,shared,uploads}/*' + '\033[39m')
        if safecommands.executeCommand("rm -rf web/{fakes,shared,uploads}/*", False) != 0:
            return 1

        print('\033[93m' + 'rm -rf app/cache/test' + '\033[39m')
        if safecommands.executeCommand("rm -rf app/cache/test", False) != 0:
            return 1

        print('\033[93m' + 'rm -rf app/logs/test' + '\033[39m')
        return safecommands.executeCommand('rm -rf app/logs/test', False)

    def switchTest(self):
        '''Check that the test file exists '''
        status = safecommands.executeCommand("ls " + self.test)
        return status

    def executeTest(self):
        status = safecommands.executeCommandAndGetOutput("php ./vendor/phpunit/phpunit/phpunit.php --verbose --tap -c app " + self.test)
        self.testResult = status[1]
        return status[0]

    def run(self):
        '''Main method for the slave'''
        while 1:
            request, data = self.readRequest()
            if request == self.BRANCH_SELECTION:
                self.branch = data
                if self.switchBranch() == 0:
                    self.response = self.BRANCH_OK
                else:
                    self.response = self.GENERIC_ERROR
                self.provideResponse()

            elif request == self.TEST_SELECTION:
                self.test = data
                if self.switchTest() == 0:
                    self.response = self.TEST_OK
                else:
                    self.response = self.GENERIC_ERROR
                self.provideResponse()

            elif request == self.TEST_EXEC:
                if self.executeTest() == 0:
                    self.response = self.TEST_EXEC_OK
                else:
                    self.response = self.GENERIC_ERROR
                self.provideResponse()
            elif request == self.RESULT:
                self.response = self.testResult
                self.provideResponse()
