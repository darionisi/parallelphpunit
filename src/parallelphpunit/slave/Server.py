'''
Created on 10/mag/2014

@author: Dario Nisi
'''
from parallelphpunit.NetworkInterface import NetworkInterface
import socket
import threading

class Server(NetworkInterface):
    '''
    Server: Implementation of the basic network service for the server
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.createSocket()

    def createSocket(self):
        self.socket = socket.socket()
        self.host = socket.gethostname()
        self.socket.bind(("", NetworkInterface.SERVER_PORT))

    def waitConnection(self):
        '''Listens for a new connection. Performs the handshake '''
        self.socket.listen(0)
        try:
            self.conn, self.clientAddress = self.socket.accept()
            if self.dataRiceive()[0] == NetworkInterface.CONNECT_REQUEST:
                self.dataSend(NetworkInterface.CONNECT_ACCEPT)
            else:
                self.dataSend(NetworkInterface.CONNECT_REFUSED)
            return 1
        except socket.error as e:
            print(e)
            self.dataSend(NetworkInterface.CONNECT_REFUSED)
            return 0

    def dataRiceive(self):
        '''High-level interface for socket input'''
        data =  self.conn.recv(65565).split(":", 1)
        if len(data) == 1:
            data.append("")
            
        return data[0], data[1]
    
    def dataSend(self, data):
        '''High-level interface for socket output'''
        self.conn.sendall(data)

    def closeConnection(self):
        '''Close protocol and tcp connection'''
        self.dataSend(NetworkInterface.CLOSE_CONNECTION)
        self.closeSocket()

    def closeSocket(self):
        self.conn.close()


class ServerThread(threading.Thread):
    '''
    ServerThread: the implementation of the network process
    '''
    def __init__(self, slave, sem1, sem2):
        threading.Thread.__init__(self)
        self.slave = slave
        self.semSlave = sem1
        self.semServer = sem2
        self.server = Server()
        self.test = ""
        self.response = ""

    def run(self):
        '''ServerThread main method'''
        while True:
            try:
                while not self.server.waitConnection():
                    pass

                while 1:
                    cmd, opt = self.server.dataRiceive()
                    if cmd == NetworkInterface.BRANCH_SETTING_REQUEST:
                        if not self.branchSetting(opt):
                            self.server.dataSend(NetworkInterface.BRANCH_SETTING_FAILED)
                            self.server.closeConnection()
                            break
                        self.server.dataSend(NetworkInterface.BRANCH_SETTING_SUCCEED)

                    elif cmd == NetworkInterface.TEST_SELECTION_REQUEST:
                        if not self.testSelection(opt):
                            self.server.dataSend(NetworkInterface.TEST_SELECTION_FAILED)
                            self.server.closeConnection()
                            break
                        self.server.dataSend(NetworkInterface.TEST_SELECTION_SUCCEED)

                    elif cmd == NetworkInterface.TEST_EXECUTION_REQUEST:
                        if self.test == "":
                            self.server.closeConnection()
                            break
                        if not self.testExecution():
                            self.server.dataSend(NetworkInterface.TEST_EXECUTION_FAILED)
                        else:
                            self.server.dataSend(NetworkInterface.TEST_EXECUTION_OK)

                    elif cmd == NetworkInterface.RESULT_REQUEST:
                        self.result()
                        self.server.dataSend(NetworkInterface.RESULT_RESPONSE + ":" + self.response +" [END OF FILE]")
                    else:
                        self.server.closeConnection()
                        break
            except:
                self.server.closeSocket()
                self.server.createSocket()
                
    def branchSetting(self, branch):
        '''Get the branch from the client, then passes it to the main process. Returns 1 if ok.'''
        self.slave.setRequest(self.slave.BRANCH_SELECTION)
        self.slave.setData(branch)
        self.semSlave.release()
        self.semServer.acquire()
        if self.slave.getResponse() != self.slave.BRANCH_OK:
            return 0
        else:
            return 1

    def testSelection(self, test):
        self.test = test
        self.slave.setRequest(self.slave.TEST_SELECTION)
        self.slave.setData(self.test)
        self.semSlave.release()
        self.semServer.acquire()
        if self.slave.getResponse() != self.slave.TEST_OK:
            return 0
        else:
            return 1

    def testExecution(self):
        self.slave.setRequest(self.slave.TEST_EXEC)
        self.semSlave.release()
        self.semServer.acquire()
        if self.slave.getResponse() != self.slave.TEST_EXEC_OK:
            return 0
        else:
            return 1
    
    def result(self):
        self.slave.setRequest(self.slave.RESULT)
        self.semSlave.release()
        self.semServer.acquire()
        self.response = self.slave.getResponse()