import sys
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../"))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../.."))


from threading import Semaphore
import threading
import time
from datetime import datetime
import yaml
import pprint
from parallelphpunit.ListaTest import ListaTest
from MasterThread import MasterThread
import safecommands

def validateParams(config):
    if os.path.isdir(config['project_root']) :
        os.chdir(config['project_root'])
    else:
        print("Error: directory doesn't exist")
        return None, None, None, None, None

    if 'ips' not in config :
        print("Error: no ip address weren't found")
        return None, None, None, None, None

    if 'branch' not in config :
        print("Error: branch wasn't found")
        return None, None, None, None, None

    if 'log_directory' not in config :
        print("Use default log directory (/var/log/parallelphpunit/)")
        log_dir = "/var/log/parallelphpunit/"
    else:
        log_dir = config['log_directory']
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)
    return config['ips'], config['project_root'], config['skuola_root'], config['branch'], config['log_directory']

def switchBranch(branch, root):
    if safecommands.executeCommand("cd " + root) != 0:
        return None
    if safecommands.executeCommand("git fetch") != 0:
        return None
    if safecommands.executeCommand("git checkout " + branch) != 0:
        return None
    if safecommands.executeCommand("git reset --hard origin/" + branch) != 0:
        return None
    return 1

def deleteOldLogs(log_directory):
    if safecommands.executeCommand("cd " + log_directory) != 0:
        return None
    if safecommands.executeCommand("rm -rf *.log", False) != 0:
        return None
    if safecommands.executeCommand("rm -rf failed passed", False) != 0:
        return None
    return 1

def createAndPopulateList(root, directory):
    l = ListaTest(root, directory)
    l.scan()
    return l

def calculateElapsedTime(start, end):
    elapsed = end - start
    hours = elapsed.seconds / 3600
    minutes = (elapsed.seconds - hours * 3600)/60
    seconds = elapsed.seconds - hours * 3600 - minutes * 60
    return hours, minutes, seconds


#----------Start main method------------------------#

f = open('config.yml')
config = yaml.safe_load(f)

ips, root, directory, branch, log_dir = validateParams(config)

if ips == None:
    exit(1)

if switchBranch(branch, root) == None or deleteOldLogs(log_dir) == None:
    exit(1)

l = createAndPopulateList(root, directory)
print("%s test files founded" % l.getCount())

sem_list = Semaphore(0)
sem_file = Semaphore(0)
childs = []
for ip in ips:
    child = MasterThread(ip, branch, l, sem_list, sem_file, log_dir)
    childs.append(child)
    child.setDaemon(True)
    child.start()

start = datetime.now()

sem_list.release()
sem_file.release()

try:
    while threading.active_count() > 1:
        time.sleep(0.1)
except KeyboardInterrupt:
    pass

end = datetime.now()
print("Elapsed time: %sH:%sm:%ss" % (calculateElapsedTime(start, end)))