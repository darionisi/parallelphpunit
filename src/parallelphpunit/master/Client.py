'''
Created on 08/mag/2014

@author: Dario Nisi
'''
from parallelphpunit.NetworkInterface import NetworkInterface
from socket import socket
from socket import error

class Client(NetworkInterface):
    '''
    Client:
        definition of the network manager for the master
    '''


    def __init__(self, ip):
        '''
        Constructor
        '''
        self.ip = ip
        self.socket = socket()
        
    def createConnection(self):
        '''High-level implementation of the connection handshake'''
        try:
            self.socket.connect((self.ip, Client.SERVER_PORT))
            self.socket.send(Client.CONNECT_REQUEST)

            response = self.socket.recv(1024);
            if response == Client.CONNECT_ACCEPT:
                return 1
            if response == Client.CONNECT_REFUSED:
                return -1
        except error as e:
            print(e)
        
        return 0
    
    def selectBranch(self, branch):
        '''High-level implementation for the branch selection network routine'''
        self.currentbranch = branch
        try:
            self.socket.sendall(Client.BRANCH_SETTING_REQUEST + ":" + branch)
            response = self.socket.recv(1024);
            if response == Client.BRANCH_SETTING_SUCCEED:
                return 1
            if response == Client.BRANCH_SETTING_FAILED:
                return 0
        except error as e:
            print(e)
            return 0
        return 0

    def selectTest(self, test):
        '''High-level implementation of the test selection network routine '''
        self.currentTest = test
        try:
            self.socket.sendall(Client.TEST_SELECTION_REQUEST + ":" + test)
            response = self.socket.recv(1024);
            if response == Client.TEST_SELECTION_SUCCEED:
                return 1
            if response == Client.TEST_SELECTION_FAILED:
                return 0
        except error as e:
            print(e)
            return 0

    def executeTest(self):
        try:
            self.socket.sendall(Client.TEST_EXECUTION_REQUEST)
            data = self.socket.recv(1024)
            if data == self.TEST_EXECUTION_OK:
                data = 1
            else:
                data = 0
            return data
        except error as e:
            print(e)
            return 0

    def resultRequest(self):
        try:
            self.socket.sendall(Client.RESULT_REQUEST)
            data = self.socket.recv(65535).split(":", 1)
            if data[0] == Client.RESULT_RESPONSE:
                while not data[1].endswith("[END OF FILE]"):
                    data[1] = data[1] + self.socket.recv(65535)
                data[1] = data[1][:data[1].rfind("[")]
                return data[1]
            else:
                return ""
        except error as e:
            print(e)
            return ""
