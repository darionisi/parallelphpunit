'''
Created on 07/mag/2014

@author: Dario Nisi
'''

import threading
from Client import Client

class MasterThread(threading.Thread):
    '''
    Thread definition for master service
    '''
    
    def __init__(self, ip, branch, l, sem_list, sem_file, log_dir):
        threading.Thread.__init__(self)
        self.ip = ip
        self.branch = branch
        self.l = l
        self.sem_list = sem_list
        self.sem_file = sem_file
        self.client = Client(self.ip)
        self.branchSync = 0
        self.testSync = 0
        self.result = ""
        self.lastTestResultCode = -1
        self.lastTestLogFile = ""
        self.log_dir = log_dir
    
    def run(self):
        '''Main method for the master process'''
        if self.connect() != 1:
            #self.writeOutput("Error: can't create connection with %s" % self.ip)
            return -1

        self.test = self.acquireListAndGetNext()
        
        i = 0
        while i < 5: #provo fino a cinque volte a sincronizzare il server sul branch desiderato
            self.selectBranch()
            if self.isServerSyncOnBranch():
                break
            i = i + 1
        else:
            #self.writeOutput("Error: slave on %s can't sync on branch %s" % (self.ip, self.branch))
            return
        
        #self.writeOutput("Selected branch %s done on %s." % (self.branch, self.ip))

        while self.test != '':
            i = 0
            while i < 5:
                self.selectTest()
                if self.isServerSyncOnTest():
                    break
                i = i + 1
            else:
                #self.writeOutput("Error: slave on %s can't select test %s" % (self.ip, self.test))
                return
            #self.writeOutput("Selected test %s on %s" % (self.test, self.ip))

            self.lastTestResultCode = self.executeTest()
            fail_dir = ''
            if self.lastTestResultCode:
                self.writeOutput(("Tests in %s " + '\033[92m' + "passed" + '\033[39m' + " on %s.") % (self.test, self.ip))
                fail_dir = 'passed/'
            else:
                self.writeOutput(("Tests in %s " + '\033[31m' + "failed" + '\033[39m' + " on %s.") % (self.test, self.ip))
                fail_dir = 'failed/'

            self.result = self.getResult()
            self.lastTestLogFile = self.log_dir + fail_dir + self.test.replace("/", "_") + ".log"
            out_file = open(self.lastTestLogFile, "w")
            out_file.write(self.result)
            out_file.close()
            #self.writeOutput("See %s for more information about test %s" % (self.lastTestLogFile, self.test))

            self.test = self.acquireListAndGetNext()
            self.testSync = 0
    
    def acquireListAndGetNext(self):
        '''Retrieve the next test from the list. Use the semaphore to lock other processes the try to use the list.'''
        self.sem_list.acquire()
        test = self.l.getNext()
        self.sem_list.release()
        return test

    def connect(self):
        '''Attempt the connection to the server on the slave '''
        return self.client.createConnection()
    
    def isServerSyncOnBranch(self):
        return self.branchSync

    def selectBranch(self):
        '''Try to set the correct branch on the slave'''
        if not self.isServerSyncOnBranch():
            self.branchSync = self.client.selectBranch(self.branch)

    def isServerSyncOnTest(self):
        return self.testSync
    
    def selectTest(self):
        '''Try to select the current test on the slave'''
        if not self.isServerSyncOnTest():
            self.testSync = self.client.selectTest(self.test)

    def executeTest(self):
        '''Execute the test on the server and get the response '''
        return self.client.executeTest()

    def getResult(self):
        return self.client.resultRequest()
    
    def writeOutput(self, stri):
        self.sem_file.acquire()
        print(stri)
        self.sem_file.release()