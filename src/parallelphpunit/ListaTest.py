'''
Created on 07/mag/2014

@author: Dario Nisi
'''

import os
import safecommands

class ListaTest(object):
    '''
    ListaTest implements the list of test that have to be executed.
    
    It provides:
    methods that scan the project in order to create the list,
    getter and setter of the list itself,
    methods that return not yet executed tests
    '''

    def __init__(self, path, src):
        '''Constructor. path: the path to the Symfony project'''
        self.path = path
        self.index = 0
        self.src = src
        
    def scan(self):
        '''Scan: create the list of tests, scanning all the directories of the Symfony project'''
        safecommands.executeCommand("cd " + self.path)
        self.list = safecommands.executeCommandAndGetOutput("find src| grep Test.php", False)[1].split('\n')
        safecommands.executeCommandAndGetOutput("cd -")

    def getList(self):
        '''Returns the list of tests'''
        return self.list
    
    
    def getCount(self):
        '''Returns the number of tests'''
        return len(self.list)
    
    def getNext(self):
        ''' Returns the next test that have to be executed'''
        if self.index == self.getCount():
            return ''
        self.index = self.index + 1
        return self.list[self.index-1] 